package instagram

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"os"
)

const instagramUserURL = "https://instagram.com"
const instagramAPIURL = "https://api.instagram.com"

// Init the module
func init() {
}

func IsValidAccount(instagramName string) (bool, error) {
	requestURL := instagramUserURL + "/" + instagramName
	resp, err := http.Get(requestURL)
	if err != nil {
		return false, fmt.Errorf("ERROR %s", string(err.Error()))
	}
	if resp.StatusCode == 404 {
		return false, nil
	}
	return true, nil
}

// func IsValidAccountGAE(instagramName string, r *http.Request) (bool, error) {
// 	requestURL := instagramUserURL + "/" + instagramName
// 	resp, err := http.Get(requestURL)
// 	ctx := appengine.NewContext(r)
// 	httpc := urlfetch.Client(ctx)
// 	resp, err = http.Get(requestURL)
// 	if err != nil {
// 		return false, fmt.Errorf("ERROR %s", string(err.Error()))
// 	}
// 	if resp.StatusCode == 404 {
// 		return false, nil
// 	}
// 	return true, nil
// }

// IGUser captures user info returned from IG auth token request
type IGUser struct {
	ID             string `json:"id"`
	Username       string `json:"username"`
	ProfilePicture string `json:"profile_picture"`
	FullName       string `json:"full_name"`
	Bio            string `json:"bio"`
	Website        string `json:"website"`
	IsBusiness     bool   `json:"is_business"`
}

type IGPaginationStruct struct {
}
type IGImageDescription struct {
	Width  int16  `json:"width"`
	Height int16  `json:"height"`
	URL    string `json:"url"`
}
type IGImage struct {
	Thumbnail          IGImageDescription `json:"thumbnail"`
	LowResolution      IGImageDescription `json:"low_resolution"`
	StandardResolution IGImageDescription `json:"standard_resolution"`
}
type IGLike struct {
	Count int16 `json:"count"`
}
type IGComments struct {
	Count int16 `json:"count"`
}
type IGRecentMediaData struct {
	ID           string     `json:"id"`
	User         IGUser     `json:"user"`
	Images       IGImage    `json:"images"`
	CreatedTime  string     `json:"created_time"`
	Caption      string     `json:"-"`
	UserHasLiked bool       `json:"user_has_liked"`
	Likes        IGLike     `json:"likes"`
	Tags         []string   `json:"tags"`
	Filter       string     `json:"filter"`
	Comments     IGComments `json:"comments"`
	MediaType    string     `json:"media_type"`
	Link         string     `json:"link"`
	Location     string     `json:"location"`
	Attribution  string     `json:"attribution"`
	UsersInPhoto []string   `json:"-"`
}
type IGMetaResponse struct {
	Code int16 `json:"code"`
}
type IGRecentMedia struct {
	Pagination IGPaginationStruct  `json:"pagination"`
	Data       []IGRecentMediaData `json:"data"`
	Meta       IGMetaResponse      `json:"meta"`
}

func GetUserRecentMedia(accessToken string) (*IGRecentMedia, error) {

	requestURL := instagramAPIURL + "/v1/users/self/media/recent?access_token="
	url := fmt.Sprintf("%s%s", requestURL, accessToken)
	// log.Printf("recent data url: %s", url)
	res, err := http.Get(url)
	if err != nil {
		return nil, fmt.Errorf("(IG) Error fetching recent media : %s", err.Error())
	}
	// body, err := ioutil.ReadAll(res.Body)
	// log.Printf("%s", body)
	defer res.Body.Close()
	userRecentMedia := IGRecentMedia{}
	err = json.NewDecoder(res.Body).Decode(&userRecentMedia)
	if err != nil {
		return nil, err
	}
	return &userRecentMedia, nil
}

// // IGAccessToken is the actual access token returned in token request
// type IGAccessToken struct {
// 	Token string `json:"access_token"`
// }

// IGOauthResponse encapsulates user and token information
type IGOauthResponse struct {
	AccessToken string `json:"access_token"`
	User        IGUser `json:"user"`
}

type IGOauthError struct {
	ErrorType    string `json:"error_type"`
	Code         int    `json:"code"`
	ErrorMessage string `json:"error_message"`
}

func AuthToken(IGCode string, req *http.Request) (*IGOauthResponse, error) {
	requestURL := instagramAPIURL + "/oauth/access_token"
	// ctx := appengine.NewContext(req)
	clientID := os.Getenv("IG_CLIENT_ID")
	clientSecret := os.Getenv("IG_CLIENT_SECRET")
	grantType := os.Getenv("IG_GRANT_TYPE")
	redirectURL := os.Getenv("IG_REDIRECT_URL")

	code := IGCode
	postData := url.Values{
		"client_id":     {clientID},
		"client_secret": {clientSecret},
		"grant_type":    {grantType},
		"code":          {code},
		"redirect_uri":  {redirectURL}}
	// log.Printf("%v", postData)
	//httpc := urlfetch.Client(ctx)
	resp, err := http.PostForm(requestURL, postData)
	if err != nil {
		return nil, fmt.Errorf("(IG) Error validating IG token: %s", err.Error())
	}
	if resp.StatusCode != 200 {
		var authError IGOauthError
		err = json.NewDecoder(resp.Body).Decode(&authError)
		if err != nil {
			return nil, err
		}
		return nil, fmt.Errorf("(IG) Error IG oauth exception %-v for code %s", authError, IGCode)
	}
	// if err != nil {
	// 	return nil, err
	// }
	defer resp.Body.Close()
	authResponse := IGOauthResponse{}
	err = json.NewDecoder(resp.Body).Decode(&authResponse)
	if err != nil {
		return nil, err
	}
	return &authResponse, nil
}
