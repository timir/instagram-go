package instagram

import (
	"strconv"
	"testing"
)

// TestIsValidAccount checks for a 404 when requesting Instagram account
func TestIsValidAccount(t *testing.T) {
	validAccountName := "tkaria"
	invalidAccountName := "ldsjfalsdjflksafjlsdjflsoiuuou"
	res, err := IsValidAccount(validAccountName)
	if err != nil {
		t.Fatalf("%s", err.Error())
	}
	if res != true {
		t.Error("Checking Instagram account: " + validAccountName + " returned " + strconv.FormatBool(res) + " expected true")
	}
	res, err = IsValidAccount(invalidAccountName)
	if err != nil {
		t.Fatalf("%s", err.Error())
	}
	if res != false {
		t.Error("Checking Instagram account: " + invalidAccountName + " returned " + strconv.FormatBool(res) + " expected false")
	}
}
